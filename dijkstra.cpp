#include <stdio.h>
#include <limits.h>
#define V 9
  


// Hen�z en k�sa yol a�ac�na dahil edilmemi� k��eler k�mesinden, minimum uzakl�k de�eriyle tepe noktas�n� bulmak i�in bir yard�mc� fonksiyon.
int minDistance(int dist[], bool sptSet[])
{
  //Min degerini baslat
   int min = INT_MAX, min_index;
  
   for (int v = 0; v < V; v++)
     if (sptSet[v] == false && dist[v] <= min)
         min = dist[v], min_index = v;
  
   return min_index;
}
  
//Olu�turulan mesafe dizisini basmak i�in bir fayda fonksiyonu.
int printSolution(int dist[], int n)
{
   printf("Vertex Kaynaktan Uzakl�k\n");
   for (int i = 0; i < V; i++)
      printf("%d \t\t %d\n", i, dist[i]);
}
  
//Yak�nsakl�k matrisi temsilini kullanarak temsil edilen bir grafik i�in Dijkstra'n�n tek kaynakl� en k�sa yol algoritmas�n� uygulayan i�lev
void dijkstra(int graph[V][V], int src)
{
     int dist[V];     //��kt� dizisi. Dist [i] src'den i'ye olan en k�sa mesafeyi korur
  
     bool sptSet[V]; //SptSet [i], vertex i en k�sa yol a�ac�na dahil edilmi�se veya src'den i'ye en k�sa mesafede bulunmas� durumunda do�ru olur
  
     
     for (int i = 0; i < V; i++)
        dist[i] = INT_MAX, sptSet[i] = false;
  
     //Kaynak k��enin kendinden uzakl��� daima 0
     dist[src] = 0;
  
    //T�m k��eler i�in en k�sa yolu bulun
     for (int count = 0; count < V-1; count++)
     {
       //Hen�z i�lenmemi� k��eler k�mesinden en d���k uzakl�k vertexini se�in. U, ilk tekrarlamada daima src'ye e�ittir.
       int u = minDistance(dist, sptSet);
  
      //Se�ilen k��eyi i�lenmi� olarak i�aretleyin.
       sptSet[u] = true;
  
      //Se�ilen k��enin biti�ik k��elerinin dist de�erini g�ncelleyin.
       for (int v = 0; v < V; v++)
         //G�ncel dist [v] yaln�zca sptSet'de de�ilse, u'dan v'e bir kenar bulunur ve src'den v'den u'ya olan toplam yol a��rl��� dist [v] 'in ge�erli de�erinden daha k���kt�r.
         if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX 
                                       && dist[u]+graph[u][v] < dist[v])
            dist[v] = dist[u] + graph[u][v];
     }
  
 //Olu�turulan mesafe dizisini yazd�rabilirsiniz.
     printSolution(dist, V);
}
  //Yukar�daki i�levi test etmek i�in s�r�c� program�.
int main()
{
//Yukar�da tart���lan �rnek grafi�i olu�tural�m.
   int graph[V][V] = {{0, 4, 0, 0, 0, 0, 0, 8, 0},
                      {4, 0, 8, 0, 0, 0, 0, 11, 0},
                      {0, 8, 0, 7, 0, 4, 0, 0, 2},
                      {0, 0, 7, 0, 9, 14, 0, 0, 0},
                      {0, 0, 0, 9, 0, 10, 0, 0, 0},
                      {0, 0, 4, 14, 10, 0, 2, 0, 0},
                      {0, 0, 0, 0, 0, 2, 0, 1, 6},
                      {8, 11, 0, 0, 0, 0, 1, 0, 7},
                      {0, 0, 2, 0, 0, 0, 6, 7, 0}
                     };
  
    dijkstra(graph, 0);
  
    return 0;
}
